from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.exceptions import APIException


class APIBadRequest(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('A bad request error occurred.')


class APINotFound(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('A not found error occurred.')
