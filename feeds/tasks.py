from __future__ import absolute_import, unicode_literals

from time import sleep

import feedparser
from celery import shared_task

from feeds.models import Feed


@shared_task
def rss_feeds():
    print('collecting Rss feeds...')

    urls = ['http://rss.cnn.com/rss/cnn_topstories.rss']
    for url in urls:
        feeds = feedparser.parse(url)
        for entry in feeds.entries:
            Feed.objects.create(
                title=entry.get('title', ''),
                published_date=entry.get('published', ''),
                link=entry.get('link', '')
            )
    sleep(5)


rss_feeds()
