from django.db import models
from model_utils.models import TimeStampedModel

from accounts.models import User


class Feed(TimeStampedModel):
    title = models.CharField(max_length=2000, blank=True, null=True)
    link = models.CharField(max_length=2000)
    published_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class UserFeed(TimeStampedModel):
    """
    FeedItem model stores  user's feed items
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE)
    read_flag = models.BooleanField(default=False)
    favorite_flag = models.BooleanField(default=False)
    comment = models.CharField(max_length=2000, blank=True, null=True)

    def __str__(self):
        return '{user} ({feed})'.format(user=self.user, feed=self.feed)
