from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from accounts.models import User
from feeds.models import Feed


class FeedTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(email='farideh@farideh', password='123456', username='farideh@farideh')
        self.token = Token.objects.get(user=self.user)
        self.client.force_login(user=self.user)

        data_list = [
            {
                "title": "The re-emergence of charming 'little wine holes' in Florence",
                "link": "http://rss.cnn.com/~r/rss/cnn_topstories/~3/zDFVH4x6GBg/index.html",
                "published_date": "2020-08-12T19:19:29.077516Z"
            },
            {
                "title": "More than 100 Black men urge Biden to pick Black woman as vice president",
                "link": "http://rss.cnn.com/~r/rss/cnn_topstories/~3/u5a5BiVEKSU/index.html",
                "published_date": "2020-08-12T19:19:29.066405Z"
            },
        ]
        for data in data_list:
            Feed.objects.create(**data)

    def test_feed_list(self):
        # get list of feeds successful
        response = self.client.get('/api/feeds/', HTTP_AUTHORIZATION='Token ' + str(self.token))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_feed_retrieve(self):
        response = self.client.get('/api/feeds/10/', HTTP_AUTHORIZATION='Token ' + str(self.token))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        response = self.client.get('/api/feeds/3/', HTTP_AUTHORIZATION='Token ' + str(self.token))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_feed_list_create(self):
        data = {
            "id": (Feed.objects.all().first()).id
        }
        response = self.client.post('/api/userfeeds/', data=data, HTTP_AUTHORIZATION='Token ' + str(self.token))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get('/api/userfeeds/', HTTP_AUTHORIZATION='Token ' + str(self.token))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_feed_update(self):
        data = {
            "comment": "this is a comment"
        }
        response = self.client.put('/api/userfeeds/1008/', data=data, HTTP_AUTHORIZATION='Token ' + str(self.token))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
