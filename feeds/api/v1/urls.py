from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

from feeds.api.v1.views import FeedListView, UserFeedListCreateView, FeedRetrieveView, UserFeedUpdateView

urlpatterns = {
    url(r'^', include([
        url(r'^feeds/$', FeedListView.as_view()),
        url(r'^feeds/(?P<id>[-\w]+)/$', FeedRetrieveView.as_view()),
        url(r'^userfeeds/$', UserFeedListCreateView.as_view()),
        url(r'^userfeeds/(?P<feed_id>[-\w]+)/$', UserFeedUpdateView.as_view()),
    ]))
}

urlpatterns = format_suffix_patterns(urlpatterns)
