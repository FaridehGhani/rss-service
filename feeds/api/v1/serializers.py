from rest_framework import serializers


class FeedSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    title = serializers.CharField(required=True)
    link = serializers.CharField(required=True)
    published_date = serializers.DateTimeField(required=True)


class UserFeedSerializer(serializers.Serializer):
    # feed_id
    id = serializers.IntegerField(required=False)
    read_flag = serializers.BooleanField(required=False)
    favorite_flag = serializers.BooleanField(required=False)
    comment = serializers.CharField(required=False)
