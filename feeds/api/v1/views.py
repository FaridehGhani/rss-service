from django.utils.translation import gettext_lazy as _
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from feeds.api.v1.serializers import FeedSerializer, UserFeedSerializer
from feeds.models import Feed, UserFeed
from src.exceptions import APIBadRequest, APINotFound


class FeedListView(generics.ListAPIView):
    """ List feed entries based on the latest """
    http_method_names = ['get']
    serializer_class = FeedSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        feeds = Feed.objects.order_by('-created')

        return Response({
            'data': FeedSerializer(feeds, many=True).data,
            'message': _('all feeds retrieved successfully.')
        }, status.HTTP_200_OK)


class FeedRetrieveView(generics.RetrieveUpdateAPIView):
    """ retrieve single feed entry """
    http_method_names = ['get']
    serializer_class = FeedSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            feed = Feed.objects.get(id=kwargs['id'])
        except Feed.DoesNotExist:
            raise APINotFound(_('no feed found.'))

        try:
            user_feed = UserFeed.objects.get(feed_id=kwargs['id'])
            user_feed.read_flag = True
            user_feed.save()
        except UserFeed.DoesNotExist:
            new_user_feed = UserFeed.objects.create(user=request.user, feed=Feed.objects.get(id=kwargs['id']))
            new_user_feed.read_flag = True
            new_user_feed.save()

        return Response({
            'data': FeedSerializer(feed).data,
            'message': _('feed retrieved successfully.')
        }, status.HTTP_200_OK)


class UserFeedListCreateView(generics.ListCreateAPIView):
    http_method_names = ['get', 'post']
    serializer_class = UserFeedSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """ create userfeed item based on selected feed by user """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        try:
            if UserFeed.objects.filter(feed_id=data['id']).exists():
                return Response({
                    'message': _('feed item is created successfully.')
                }, status.HTTP_201_CREATED)
            else:
                UserFeed.objects.create(user=request.user, feed=Feed.objects.get(id=data['id']))
        except Exception:
            raise APIBadRequest(_('feed item not created.'))

        return Response({
            'message': _('feed item is created successfully.')
        }, status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):
        """ retrieve userfeed's item """
        feed_ids = [user_feed.feed_id for user_feed
                    in UserFeed.objects.filter(user=request.user).all()]
        feeds = [feed for feed
                 in Feed.objects.all()
                 if feed.id in feed_ids]

        return Response({
            'data': FeedSerializer(feeds, many=True).data,
            'message': _('userfeeds retrieved successfully.')
        }, status.HTTP_200_OK)


class UserFeedUpdateView(generics.RetrieveUpdateAPIView):
    """ update user feed in order to add comment or make it favorite """
    http_method_names = ['put']
    serializer_class = UserFeedSerializer
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        if not UserFeed.objects.filter(user=request.user).exists():
            raise APINotFound(_('no feed found for user.'))
        UserFeed.objects.filter(feed_id=kwargs['feed_id']).update(**data)

        return Response({
            'data': data,
            'message': _('feed item updated successfully.')
        }, status.HTTP_200_OK)
