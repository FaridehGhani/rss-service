from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from rest_framework.authtoken.models import Token


class User(AbstractUser):
    """
    User model stores users information
    """
    email = models.EmailField(_('email address'), blank=False, null=False, unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.email


@receiver(post_save, sender=User)
def create_token(sender, instance=None, created=None, **kwargs):
    if created:
        Token.objects.create(user=instance)
