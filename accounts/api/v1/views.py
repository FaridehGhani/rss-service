from django.utils.translation import gettext_lazy as _
from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from accounts.api.v1.serializers import RegisterSerializer
from accounts.models import User
from src.exceptions import APIBadRequest


class RegisterView(generics.CreateAPIView):
    """ Register new user """
    http_method_names = ['post']
    serializer_class = RegisterSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        if User.objects.filter(email=data['email']).exists():
            raise APIBadRequest(_('user already exists'))
        try:
            User.objects.create_user(**data, username=data['email'])
        except Exception:
            raise APIBadRequest(_('user not registered.'))

        return Response({
            'message': _('user registered successfully.')
        }, status.HTTP_201_CREATED)
