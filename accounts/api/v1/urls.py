from django.conf.urls import url, include
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.urlpatterns import format_suffix_patterns

from accounts.api.v1.views import RegisterView

urlpatterns = {
    url(r'^', include([
        url(r'^accounts/register/$', RegisterView.as_view()),
        url(r'^accounts/login/$', obtain_auth_token),
    ]))
}

urlpatterns = format_suffix_patterns(urlpatterns)
