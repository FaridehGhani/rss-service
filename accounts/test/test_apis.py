from rest_framework import status
from rest_framework.test import APITestCase


class RegisterLoginUserTestCase(APITestCase):
    def test_register_login(self):
        register_url = '/api/accounts/register/'
        login_url = '/api/accounts/login/'

        register_data = {
            "email": "test@test.com",
            "password": "test"
        }
        login_data = {
            "username": "test@test.com",
            "password": "test"
        }
        invalid_login_data = {
            "username": "test@test.com",
            "password": "test_"
        }
        # successful registration
        response = self.client.post(register_url, register_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # register with duplicate data
        response = self.client.post(register_url, register_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # login with valid credentials
        response = self.client.post(login_url, login_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # login with invalid credentials
        response = self.client.post(login_url, invalid_login_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
