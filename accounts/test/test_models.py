from django.test import TestCase

from accounts.models import User


class UserTestCase(TestCase):
    def test_user_str(self):
        data = {
            "email": "test@test.com",
            "password": "test"
        }
        user = User.objects.create(**data, username=data['email'])
        self.assertEqual(str(user), user.email)
